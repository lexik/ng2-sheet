import { EventEmitter, OnDestroy, OnInit } from '@angular/core';
import { SheetService } from '../sheet/sheet.service';
import { Subscription } from 'rxjs';
export declare class SheetListComponent implements OnInit, OnDestroy {
    private sheetService;
    sheetList: any[];
    closeSheetSource$: Subscription;
    onComponentCreated: EventEmitter<any>;
    constructor(sheetService: SheetService);
    ngOnInit(): void;
    addSheet(componentType: any, name: any, params?: any): void;
    componentCreated(emitComponent: any): void;
    closeSheet(): void;
    ngOnDestroy(): void;
}
