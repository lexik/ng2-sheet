"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var sheet_list_component_1 = require("./sheet-list.component");
var SheetListService = (function () {
    function SheetListService(componentFactoryResolver) {
        this.componentFactoryResolver = componentFactoryResolver;
    }
    SheetListService.prototype.init = function (vCref) {
        vCref.clear();
        var factory = this.componentFactoryResolver.resolveComponentFactory(sheet_list_component_1.SheetListComponent);
        var injector = core_1.ReflectiveInjector.fromResolvedProviders([], vCref.parentInjector);
        var ref = factory.create(injector);
        vCref.insert(ref.hostView);
        this.componentRef = ref.instance;
    };
    SheetListService.prototype.getComponent = function () {
        return this.componentRef;
    };
    SheetListService.prototype.ngOnDestroy = function () {
        this.componentRef.destroy();
    };
    SheetListService.decorators = [
        { type: core_1.Injectable },
    ];
    /** @nocollapse */
    SheetListService.ctorParameters = function () { return [
        { type: core_1.ComponentFactoryResolver, },
    ]; };
    return SheetListService;
}());
exports.SheetListService = SheetListService;
//# sourceMappingURL=sheet-list.service.js.map