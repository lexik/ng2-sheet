import { AfterViewInit, ChangeDetectorRef, Component, ComponentFactoryResolver, ComponentRef, EventEmitter, OnChanges, OnDestroy, OnInit, Type } from '@angular/core';
import { SheetService } from './sheet.service';
export interface Config {
    type: Type<Component>;
    componentName: string;
    inputParams: any;
}
export declare class SheetComponent implements OnInit, AfterViewInit, OnChanges, OnDestroy {
    private componentFactoryResolver;
    private _changeDetectionRef;
    private sheetService;
    sheet: any;
    onComponentCreate: EventEmitter<any>;
    onCloseModal: EventEmitter<any>;
    config: Config;
    cmpRef: ComponentRef<Component>;
    private isViewInitialized;
    constructor(componentFactoryResolver: ComponentFactoryResolver, _changeDetectionRef: ChangeDetectorRef, sheetService: SheetService);
    ngOnInit(): void;
    ngAfterViewInit(): void;
    updateComponent(): void;
    closeSheet(): void;
    ngOnChanges(): void;
    ngOnDestroy(): void;
}
