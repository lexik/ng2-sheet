"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var sheet_list_component_1 = require("./sheet-list/sheet-list.component");
var sheet_component_1 = require("./sheet/sheet.component");
var sheet_list_service_1 = require("./sheet-list/sheet-list.service");
var common_1 = require("@angular/common");
var SheetModule = (function () {
    function SheetModule() {
    }
    SheetModule.decorators = [
        { type: core_1.NgModule, args: [{
                    imports: [
                        common_1.CommonModule,
                    ],
                    declarations: [
                        sheet_list_component_1.SheetListComponent, sheet_component_1.SheetComponent
                    ],
                    exports: [
                        sheet_list_component_1.SheetListComponent, sheet_component_1.SheetComponent
                    ],
                    providers: [
                        sheet_list_service_1.SheetListService
                    ],
                    entryComponents: [sheet_list_component_1.SheetListComponent]
                },] },
    ];
    /** @nocollapse */
    SheetModule.ctorParameters = function () { return []; };
    return SheetModule;
}());
exports.SheetModule = SheetModule;
//# sourceMappingURL=sheet.module.js.map