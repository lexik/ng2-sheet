"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var sheet_service_1 = require("./sheet.service");
var SheetComponent = (function () {
    function SheetComponent(componentFactoryResolver, _changeDetectionRef, sheetService) {
        this.componentFactoryResolver = componentFactoryResolver;
        this._changeDetectionRef = _changeDetectionRef;
        this.sheetService = sheetService;
        this.onComponentCreate = new core_1.EventEmitter();
        this.onCloseModal = new core_1.EventEmitter();
        this.isViewInitialized = false;
    }
    SheetComponent.prototype.ngOnInit = function () { };
    SheetComponent.prototype.ngAfterViewInit = function () {
        this.sheetService.updateSheetWidth();
        this.isViewInitialized = true;
        this.updateComponent();
        this._changeDetectionRef.detectChanges();
    };
    SheetComponent.prototype.updateComponent = function () {
        if (!this.isViewInitialized) {
            return;
        }
        this.sheet.clear();
        var factory = this.componentFactoryResolver.resolveComponentFactory(this.config.type);
        this.cmpRef = this.sheet.createComponent(factory);
        var emitComponent = { cmp: this.cmpRef, name: this.config.componentName, params: this.config.inputParams };
        var component = emitComponent.cmp;
        var paramsOptional = emitComponent.params;
        if (Object.keys(paramsOptional).length !== 0) {
            var keysParams = Object.keys(paramsOptional);
            for (var _i = 0, keysParams_1 = keysParams; _i < keysParams_1.length; _i++) {
                var key = keysParams_1[_i];
                (component.instance)[key] = paramsOptional[key];
            }
        }
        this.onComponentCreate.emit(emitComponent);
    };
    SheetComponent.prototype.closeSheet = function () {
        this.onCloseModal.emit(true);
    };
    SheetComponent.prototype.ngOnChanges = function () {
        this.updateComponent();
    };
    SheetComponent.prototype.ngOnDestroy = function () {
        this.cmpRef.destroy();
    };
    SheetComponent.decorators = [
        { type: core_1.Component, args: [{
                    selector: '[widget-sheet]',
                    template: "\n      <div class=\"sheet-overlay\" (click)=\"closeSheet()\"></div>\n      <div class=\"sheet\">\n          <ng-template #sheet></ng-template>\n      </div>\n  ",
                    providers: [sheet_service_1.SheetService]
                },] },
    ];
    /** @nocollapse */
    SheetComponent.ctorParameters = function () { return [
        { type: core_1.ComponentFactoryResolver, },
        { type: core_1.ChangeDetectorRef, },
        { type: sheet_service_1.SheetService, },
    ]; };
    SheetComponent.propDecorators = {
        'sheet': [{ type: core_1.ViewChild, args: ["sheet", { read: core_1.ViewContainerRef },] },],
        'onComponentCreate': [{ type: core_1.Output },],
        'onCloseModal': [{ type: core_1.Output },],
        'config': [{ type: core_1.Input },],
    };
    return SheetComponent;
}());
exports.SheetComponent = SheetComponent;
//# sourceMappingURL=sheet.component.js.map