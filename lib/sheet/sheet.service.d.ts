import { Subject } from 'rxjs';
export declare class SheetService {
    maxWidth: any;
    transform: any;
    closeSubject: Subject<any>;
    constructor();
    updateSheetWidth(hide?: boolean): void;
    closeSheet(): void;
    display(): void;
    private calculWidth();
}
