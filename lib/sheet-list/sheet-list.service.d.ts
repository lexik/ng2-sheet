import { ComponentFactoryResolver, ViewContainerRef, OnDestroy } from '@angular/core';
export declare class SheetListService implements OnDestroy {
    private componentFactoryResolver;
    private componentRef;
    constructor(componentFactoryResolver: ComponentFactoryResolver);
    init(vCref: ViewContainerRef): void;
    getComponent(): any;
    ngOnDestroy(): void;
}
