"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var rxjs_1 = require("rxjs");
var SheetService = (function () {
    function SheetService() {
        this.closeSubject = new rxjs_1.Subject();
    }
    SheetService.prototype.updateSheetWidth = function (hide) {
        if (hide === void 0) { hide = true; }
        if (hide) {
            jQuery('.sheet-container:nth-last-of-type(2)').find('.sheet').css('display', 'none');
        }
        this.calculWidth();
        jQuery('.sheet-container:nth-last-of-type(2)').find('.sheet').css('transform', "translate(" + jQuery(document).width() + "px, 0px)");
        jQuery('.sheet-container:nth-last-of-type(2)').find('.sheet').css('max-width', this.maxWidth);
    };
    SheetService.prototype.closeSheet = function () {
        var _this = this;
        this.calculWidth();
        jQuery('.sheet-container:nth-last-of-type(2)').find('.sheet-overlay').fadeOut(150);
        var self = this;
        jQuery('.sheet-container:nth-last-of-type(2)').find('.sheet').css('transform', "translate(" + jQuery(document).width() + "px, 0px)");
        jQuery('.sheet-container:nth-last-of-type(3)').find('.sheet').css('transform', "translate(" + self.transform + "px, 0px)");
        setTimeout(function () {
            _this.closeSubject.next(true);
        }, 500);
    };
    SheetService.prototype.display = function () {
        var _this = this;
        this.calculWidth();
        jQuery('.sheet-container:nth-last-of-type(2)').find('.sheet').css('display', '');
        setTimeout(function () {
            jQuery('.sheet-container:nth-last-of-type(2)').find('.sheet').css('transform', "translate(" + _this.transform + "px, 0px)");
        }, 100);
        jQuery('.sheet-container:nth-last-of-type(3)').find('.sheet').css('transform', "translate(0px, 0px)");
    };
    SheetService.prototype.calculWidth = function () {
        var width = jQuery(document).width();
        var maxWidth = 0;
        var minWidth = Math.floor((width - 960) / 2);
        var padding = 0;
        maxWidth ?
            (maxWidth = Math.max(maxWidth, minWidth), padding = Math.max(width - maxWidth, 0), width = Math.min(maxWidth, width))
            :
                1160 <= width ? (padding = Math.floor((width - 960) / 2), width -= padding) : 768 <= width && (padding = Math.floor((width - 768) / 2), width -= padding);
        this.maxWidth = width;
        this.transform = padding;
    };
    SheetService.decorators = [
        { type: core_1.Injectable },
    ];
    /** @nocollapse */
    SheetService.ctorParameters = function () { return []; };
    return SheetService;
}());
exports.SheetService = SheetService;
//# sourceMappingURL=sheet.service.js.map