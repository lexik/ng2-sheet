"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var sheet_service_1 = require("../sheet/sheet.service");
var SheetListComponent = (function () {
    function SheetListComponent(sheetService) {
        this.sheetService = sheetService;
        this.sheetList = [];
        this.onComponentCreated = new core_1.EventEmitter();
    }
    SheetListComponent.prototype.ngOnInit = function () { };
    SheetListComponent.prototype.addSheet = function (componentType, name, params) {
        var toInsert = {
            type: componentType,
            componentName: name,
            inputParams: params || {}
        };
        this.sheetList.push(toInsert);
    };
    SheetListComponent.prototype.componentCreated = function (emitComponent) {
        this.onComponentCreated.emit(emitComponent);
        this.sheetService.display();
    };
    SheetListComponent.prototype.closeSheet = function () {
        var _this = this;
        this.closeSheetSource$ = this.sheetService.closeSubject
            .take(1)
            .subscribe(function () {
            _this.sheetList.pop();
        });
        this.sheetService.closeSheet();
    };
    SheetListComponent.prototype.ngOnDestroy = function () {
        this.closeSheetSource$ && this.closeSheetSource$.unsubscribe();
    };
    SheetListComponent.decorators = [
        { type: core_1.Component, args: [{
                    selector: 'app-sheet-list',
                    template: "\n      <div class=\"sheet-holder\">\n          <div widget-sheet *ngFor=\"let sheet of sheetList\" class=\"sheet-container\"\n               [config]=\"sheet\"\n               (onComponentCreate)=\"componentCreated($event)\"\n               (onCloseModal)=\"closeSheet($event)\">\n          </div>\n\n          <div class=\"sheet-container\">\n              <div class=\"sheet-overlay\"></div>\n              <div class=\"sheet\"></div>\n          </div>\n      </div>\n  ",
                    providers: [sheet_service_1.SheetService],
                },] },
    ];
    /** @nocollapse */
    SheetListComponent.ctorParameters = function () { return [
        { type: sheet_service_1.SheetService, },
    ]; };
    SheetListComponent.propDecorators = {
        'onComponentCreated': [{ type: core_1.Output },],
    };
    return SheetListComponent;
}());
exports.SheetListComponent = SheetListComponent;
//# sourceMappingURL=sheet-list.component.js.map